package protect.mybudgets;

public class MyBudgets
{
    public final String nome;
    public final int max;
    public final int elementoAttuale;

    public MyBudgets(final String nome, final int max, final int value)
    {
        this.nome = nome;
        this.max = max;
        this.elementoAttuale = value;
    }
}
