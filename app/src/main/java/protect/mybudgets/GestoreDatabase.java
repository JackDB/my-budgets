package protect.mybudgets;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;


class GestoreDatabase extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "MyBudgets.db";

    public static final int ORIGINAL_DATABASE_VERSION = 1;
    public static final int DATABASE_VERSION = 2;


    static class BudgetDbIds
    {
        public static final String TABELLA = "obiettivi";
        public static final String NOME = "_id";
        public static final String MAX = "max";
    }


    static class TransactionDbIds
    {
        public static final String TABELLA = "transazioni";
        public static final String NOME = "_id";
        public static final String TIPO = "tipo";
        public static final String DESCRIZIONE = "descrizione";
        public static final String ACCOUNT = "account";
        public static final String OBIETTIVO = "obiettivo";
        public static final String VALORE = "valore";
        public static final String NOTE = "note";
        public static final String DATE = "date";
        public static final String RICETTA = "ricetta";

        public static final int SPESA = 1;
        public static final int GUADAGNO = 2;
    }

    private final Context _context;

    public GestoreDatabase(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        _context = context;
    }


    private void inviaNotificaCambiamenti()
    {
        _context.sendBroadcast(new Intent(CheckerModificheDB.DATABASE_CAMBIATO));
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // Crea Tabella Obiettivi
        db.execSQL(
                "create table  " + BudgetDbIds.TABELLA + "(" +
                        BudgetDbIds.NOME + " text primary key," +
                        BudgetDbIds.MAX + " INTEGER not null)");
       // Creea Tabella Transazioni
        db.execSQL("create table " + TransactionDbIds.TABELLA + "(" +
                TransactionDbIds.NOME + " INTEGER primary key autoincrement," +
                TransactionDbIds.TIPO + " INTEGER not null," +
                TransactionDbIds.DESCRIZIONE + " TEXT not null," +
                TransactionDbIds.ACCOUNT + " TEXT," +
                TransactionDbIds.OBIETTIVO + " TEXT," +
                TransactionDbIds.VALORE + " REAL not null," +
                TransactionDbIds.NOTE + " TEXT," +
                TransactionDbIds.DATE + " INTEGER not null," +
                TransactionDbIds.RICETTA + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

        if(oldVersion < 2 && newVersion >= 2)
        {
            db.execSQL("ALTER TABLE " + TransactionDbIds.TABELLA
                + " ADD COLUMN " + TransactionDbIds.RICETTA + " TEXT");
        }
    }


    public boolean inserisciObiettivo(final String name, final int max)
    {
        SQLiteDatabase db = getWritableDatabase();
        boolean result = inserisciObiettivo(db, name, max);
        db.close();

        return result;
    }


    public boolean inserisciObiettivo(SQLiteDatabase writableDb, final String name, final int max)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BudgetDbIds.NOME, name);
        contentValues.put(BudgetDbIds.MAX, max);

        final long newId = writableDb.insert(BudgetDbIds.TABELLA, null, contentValues);
        return (newId != -1);
    }


    public boolean aggiornaObiettivo(final String name, final int max)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BudgetDbIds.MAX, max);

        SQLiteDatabase db = getWritableDatabase();
        int rowsUpdated = db.update(BudgetDbIds.TABELLA, contentValues, BudgetDbIds.NOME + "=?",
                new String[]{name});
        db.close();

        return (rowsUpdated == 1);
    }


    public boolean rimuoviObiettivo(final String name)
    {
        SQLiteDatabase db = getWritableDatabase();
        int rowsDeleted =  db.delete(BudgetDbIds.TABELLA,
                BudgetDbIds.NOME + " = ? ",
                new String[]{name});
        db.close();
        return (rowsDeleted == 1);
    }


    public MyBudgets ottieniObiettivo(final String name)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor data = db.rawQuery("select * from " + BudgetDbIds.TABELLA +
                " where " + BudgetDbIds.NOME + "=?", new String[]{name});

        MyBudgets myBudgets = null;

        if(data.getCount() == 1)
        {
            data.moveToFirst();
            String goalName = data.getString(data.getColumnIndexOrThrow(BudgetDbIds.NOME));
            int goalMax = data.getInt(data.getColumnIndexOrThrow(BudgetDbIds.MAX));

            myBudgets = new MyBudgets(goalName, goalMax, 0);
        }

        data.close();
        db.close();

        return myBudgets;
    }


    public List<MyBudgets> ottieniTuttiObiettivi(long startDateMs, long endDateMs)
    {
        SQLiteDatabase db = getReadableDatabase();

        final String TOTAL_EXPENSE_COL = "total_expense";
        final String TOTAL_REVENUE_COL = "total_revenue";

        final String BUDGET_ID = BudgetDbIds.TABELLA + "." + BudgetDbIds.NOME;
        final String BUDGET_MAX = BudgetDbIds.TABELLA + "." + BudgetDbIds.MAX;
        final String TRANS_VALUE = TransactionDbIds.TABELLA + "." + TransactionDbIds.VALORE;
        final String TRANS_TYPE = TransactionDbIds.TABELLA + "." + TransactionDbIds.TIPO;
        final String TRANS_DATE = TransactionDbIds.TABELLA + "." + TransactionDbIds.DATE;
        final String TRANS_BUDGET = TransactionDbIds.TABELLA + "." + TransactionDbIds.OBIETTIVO;

        Cursor data = db.rawQuery("select " + BUDGET_ID + ", " + BUDGET_MAX + ", " +
                "(select total(" + TRANS_VALUE + ") from " + TransactionDbIds.TABELLA + " where " +
                    BUDGET_ID + " = " + TRANS_BUDGET + " and " +
                    TRANS_TYPE + " = ? and " +
                    TRANS_DATE + " >= ? and " +
                    TRANS_DATE + " <= ?) " +
                    "as " + TOTAL_EXPENSE_COL + ", " +
                "(select total(" + TRANS_VALUE + ") from " + TransactionDbIds.TABELLA + " where " +
                    BUDGET_ID + " = " + TRANS_BUDGET + " and " +
                    TRANS_TYPE + " = ? and " +
                    TRANS_DATE + " >= ? and " +
                    TRANS_DATE + " <= ?) " +
                    "as " + TOTAL_REVENUE_COL + " " +
                "from " + BudgetDbIds.TABELLA + " order by " + BUDGET_ID,
                new String[]
                    {
                        Integer.toString(TransactionDbIds.SPESA),
                        Long.toString(startDateMs),
                        Long.toString(endDateMs),
                        Integer.toString(TransactionDbIds.GUADAGNO),
                        Long.toString(startDateMs),
                        Long.toString(endDateMs)
                    });

        LinkedList<MyBudgets> myBudgets = new LinkedList<>();


        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(startDateMs);
        final int MONTHS_PER_YEAR = 12;
        int startMonths = date.get(Calendar.YEAR) * MONTHS_PER_YEAR + date.get(Calendar.MONTH);
        date.setTimeInMillis(endDateMs);
        int endMonths = date.get(Calendar.YEAR) * MONTHS_PER_YEAR + date.get(Calendar.MONTH);
        int totalMonthsInRange = endMonths - startMonths + 1;

        if(data.moveToFirst())
        {
            do
            {
                String name = data.getString(data.getColumnIndexOrThrow(BudgetDbIds.NOME));
                int max = data.getInt(data.getColumnIndexOrThrow(BudgetDbIds.MAX)) * totalMonthsInRange;
                double expenses = data.getDouble(data.getColumnIndexOrThrow(TOTAL_EXPENSE_COL));
                double revenues = data.getDouble(data.getColumnIndexOrThrow(TOTAL_REVENUE_COL));
                double current = expenses - revenues;
                int currentRounded = (int)Math.ceil(current);

                myBudgets.add(new MyBudgets(name, max, currentRounded));
            } while(data.moveToNext());
        }

        data.close();
        db.close();

        return myBudgets;
    }


    public MyBudgets ottieniObiettivoVuoto(long startDateMs, long endDateMs)
    {
        SQLiteDatabase db = getReadableDatabase();

        final String TOTAL_EXPENSE_COL = "total_expense";
        final String TOTAL_REVENUE_COL = "total_revenue";

        final String TRANS_VALUE = TransactionDbIds.TABELLA + "." + TransactionDbIds.VALORE;
        final String TRANS_TYPE = TransactionDbIds.TABELLA + "." + TransactionDbIds.TIPO;
        final String TRANS_DATE = TransactionDbIds.TABELLA + "." + TransactionDbIds.DATE;
        final String TRANS_BUDGET = TransactionDbIds.TABELLA + "." + TransactionDbIds.OBIETTIVO;

        Cursor data = db.rawQuery("select " +
                        "(select total(" + TRANS_VALUE + ") from " + TransactionDbIds.TABELLA + " where " +
                        TRANS_BUDGET + " = '' and " +
                        TRANS_TYPE + " = ? and " +
                        TRANS_DATE + " >= ? and " +
                        TRANS_DATE + " <= ?) " +
                        "as " + TOTAL_EXPENSE_COL + ", " +
                        "(select total(" + TRANS_VALUE + ") from " + TransactionDbIds.TABELLA + " where " +
                        TRANS_BUDGET + " = '' and " +
                        TRANS_TYPE + " = ? and " +
                        TRANS_DATE + " >= ? and " +
                        TRANS_DATE + " <= ?) " +
                        "as " + TOTAL_REVENUE_COL,
                new String[]
                        {
                                Integer.toString(TransactionDbIds.SPESA),
                                Long.toString(startDateMs),
                                Long.toString(endDateMs),
                                Integer.toString(TransactionDbIds.GUADAGNO),
                                Long.toString(startDateMs),
                                Long.toString(endDateMs)
                        });

        int total = 0;

        if(data.moveToFirst())
        {
            int expenses = data.getInt(data.getColumnIndexOrThrow(TOTAL_EXPENSE_COL));
            int revenues = data.getInt(data.getColumnIndexOrThrow(TOTAL_REVENUE_COL));
            total = expenses - revenues;
        }

        data.close();
        db.close();

        return new MyBudgets("", 0, total);
    }


    public List<String> ottieniNomiObiettivi()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor data = db.rawQuery("select " + BudgetDbIds.NOME + " from " + BudgetDbIds.TABELLA +
                " ORDER BY " + BudgetDbIds.NOME, null);

        LinkedList<String> budgetNames = new LinkedList<>();

        if(data.moveToFirst())
        {
            do
            {
                String name = data.getString(data.getColumnIndexOrThrow(BudgetDbIds.NOME));

                budgetNames.add(name);
            } while(data.moveToNext());
        }

        data.close();
        db.close();

        return budgetNames;
    }


    public int ottieniNumeroObiettivi()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor data =  db.rawQuery("SELECT Count(*) FROM " + BudgetDbIds.TABELLA, null);

        int numItems = 0;

        if(data.getCount() == 1)
        {
            data.moveToFirst();
            numItems = data.getInt(0);
        }

        data.close();
        db.close();

        return numItems;
    }


    public boolean inserisciTransazione(final int type, final String description, final String account, final String budget,
                                        final double value, final String note, final long dateInMs, final String receipt)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TransactionDbIds.TIPO, type);
        contentValues.put(TransactionDbIds.DESCRIZIONE, description);
        contentValues.put(TransactionDbIds.ACCOUNT, account);
        contentValues.put(TransactionDbIds.OBIETTIVO, budget);
        contentValues.put(TransactionDbIds.VALORE, value);
        contentValues.put(TransactionDbIds.NOTE, note);
        contentValues.put(TransactionDbIds.DATE, dateInMs);
        contentValues.put(TransactionDbIds.RICETTA, receipt);

        SQLiteDatabase db = getWritableDatabase();
        long newId = db.insert(TransactionDbIds.TABELLA, null, contentValues);
        db.close();

        if(newId != -1)
        {
            inviaNotificaCambiamenti();
        }

        return (newId != -1);
    }


    public boolean inserisciTransazione(SQLiteDatabase writableDb, final int id, final int type, final String description, final String account, final String budget,
                                        final double value, final String note, final long dateInMs, final String receipt)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TransactionDbIds.NOME, id);
        contentValues.put(TransactionDbIds.TIPO, type);
        contentValues.put(TransactionDbIds.DESCRIZIONE, description);
        contentValues.put(TransactionDbIds.ACCOUNT, account);
        contentValues.put(TransactionDbIds.OBIETTIVO, budget);
        contentValues.put(TransactionDbIds.VALORE, value);
        contentValues.put(TransactionDbIds.NOTE, note);
        contentValues.put(TransactionDbIds.DATE, dateInMs);
        contentValues.put(TransactionDbIds.RICETTA, receipt);

        long newId = writableDb.insert(TransactionDbIds.TABELLA, null, contentValues);

        if(newId != -1)
        {
            inviaNotificaCambiamenti();
        }

        return (newId != -1);
    }


    public boolean aggiornaTransazione(final int id, final int type, final String description,
                                       final String account, final String budget, final double value,
                                       final String note, final long dateInMs, final String receipt)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TransactionDbIds.TIPO, type);
        contentValues.put(TransactionDbIds.DESCRIZIONE, description);
        contentValues.put(TransactionDbIds.ACCOUNT, account);
        contentValues.put(TransactionDbIds.OBIETTIVO, budget);
        contentValues.put(TransactionDbIds.VALORE, value);
        contentValues.put(TransactionDbIds.NOTE, note);
        contentValues.put(TransactionDbIds.DATE, dateInMs);
        contentValues.put(TransactionDbIds.RICETTA, receipt);

        SQLiteDatabase db = getWritableDatabase();
        int rowsUpdated = db.update(TransactionDbIds.TABELLA, contentValues,
                TransactionDbIds.NOME + "=?",
                new String[]{Integer.toString(id)});
        db.close();

        if(rowsUpdated == 1)
        {
            inviaNotificaCambiamenti();
        }

        return (rowsUpdated == 1);
    }


    public Transazione ottieniTransazione(final int id)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor data = db.rawQuery("select * from " + TransactionDbIds.TABELLA +
                " where " + TransactionDbIds.NOME + "=?", new String[]{Integer.toString(id)});

        Transazione transazione = null;

        if(data.getCount() == 1)
        {
            data.moveToFirst();
            transazione = Transazione.creaTransazione(data);
        }

        data.close();
        db.close();

        return transazione;
    }


    public int ottieniNumeroTransazioni(final int type)
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor data =  db.rawQuery("SELECT Count(*) FROM " + TransactionDbIds.TABELLA +
                " where " + TransactionDbIds.TIPO + "=?", new String[]{Integer.toString(type)});

        int numItems = 0;

        if(data.getCount() == 1)
        {
            data.moveToFirst();
            numItems = data.getInt(0);
        }

        data.close();
        db.close();

        return numItems;
    }


    public boolean eliminaTransazione(final int id)
    {
        SQLiteDatabase db = getWritableDatabase();
        int rowsDeleted =  db.delete(TransactionDbIds.TABELLA,
                TransactionDbIds.NOME + " = ? ",
                new String[]{Integer.toString(id)});
        db.close();

        if(rowsDeleted == 1)
        {
            inviaNotificaCambiamenti();
        }

        return (rowsDeleted == 1);
    }


    public Cursor ottieniTutteTransazioni(int type, String budget, String search, Long startDateMs, Long endDateMs)
    {
        SQLiteDatabase db = getReadableDatabase();

        LinkedList<String> args = new LinkedList<>();

        String query = "select * from " + TransactionDbIds.TABELLA + " where " +
                TransactionDbIds.TIPO + "=" + type;

        if(budget != null)
        {
            query += " AND " + TransactionDbIds.OBIETTIVO + "=?";
            args.addLast(budget);
        }

        if(search != null)
        {
            query += " AND ( ";

            String [] items = new String[]{TransactionDbIds.DESCRIZIONE, TransactionDbIds.ACCOUNT,
                    TransactionDbIds.VALORE, TransactionDbIds.NOTE};

            for(int index = 0; index < items.length; index++)
            {
                query += "( " + items[index] + " LIKE ? )";
                if(index < (items.length-1))
                {
                    query += " OR ";
                }
                args.addLast("%" + search + "%");
            }

            query += " )";
        }

        if(startDateMs != null && endDateMs != null)
        {
            query += " AND " + TransactionDbIds.DATE + " >= ? AND " +
                    TransactionDbIds.DATE + " <= ?";
            args.addLast(Long.toString(startDateMs));
            args.addLast(Long.toString(endDateMs));
        }

        query += " ORDER BY " + TransactionDbIds.DATE + " DESC";

        String [] argArray = args.toArray(new String[args.size()]);

        Cursor res =  db.rawQuery(query, argArray);
        return res;
    }


    public Cursor ottieniSpese()
    {
        return ottieniTutteTransazioni(TransactionDbIds.SPESA, null, null, null, null);
    }


    public Cursor ottieniGuadagni()
    {
        return ottieniTutteTransazioni(TransactionDbIds.GUADAGNO, null, null, null, null);
    }


    public Cursor ottieniTransazioniParticolari(Long endDate)
    {
        List<String> argList = new ArrayList<>();
        if(endDate != null)
        {
            argList.add(endDate.toString());
        }
        String [] args = argList.toArray(new String[argList.size()]);

        SQLiteDatabase db = getReadableDatabase();
        Cursor res =  db.rawQuery("select * from " + TransactionDbIds.TABELLA + " where " +
                " LENGTH(" + TransactionDbIds.RICETTA + ") > 0 " +
                (endDate != null ? " AND " + TransactionDbIds.DATE + "<=? " : ""),
                args);
        return res;
    }
}
