package protect.mybudgets;


class ExceptionFormato extends Exception
{
    public ExceptionFormato(String message)
    {
        super(message);
    }

    public ExceptionFormato(String message, Exception rootCause)
    {
        super(message, rootCause);
    }
}
