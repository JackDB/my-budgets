package protect.mybudgets;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

public class MyBudgetsActivity extends AppCompatActivity
{
    private final static String TAG = "MyBudgets";

    private GestoreDatabase _db;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.budget_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        _db = new GestoreDatabase(this);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        final ListView listaObiettivi = (ListView) findViewById(R.id.list);
        final TextView scrittaAiuto = (TextView)findViewById(R.id.helpText);

        if(_db.ottieniNumeroObiettivi() > 0)
        {
            listaObiettivi.setVisibility(View.VISIBLE);
            scrittaAiuto.setVisibility(View.GONE);
        }
        else
        {
            listaObiettivi.setVisibility(View.GONE);
            scrittaAiuto.setVisibility(View.VISIBLE);
            scrittaAiuto.setText(R.string.noBudgets);
        }

        final Calendar date = Calendar.getInstance();


        final long dateMonthEndMs = GestoreCalendario.getEndOfMonthMs(date.get(Calendar.YEAR),
                date.get(Calendar.MONTH));


        final long dateMonthStartMs = GestoreCalendario.getStartOfMonthMs(date.get(Calendar.YEAR),
                date.get(Calendar.MONTH));

        final Bundle b = getIntent().getExtras();
        final long inizioObiettivoMs = b != null ? b.getLong("inizioObiettivo", dateMonthStartMs) : dateMonthStartMs;
        final long fineObiettivoMs = b != null ? b.getLong("fineObiettivo", dateMonthEndMs) : dateMonthEndMs;

        date.setTimeInMillis(inizioObiettivoMs);
        String inizioObiettivoString = DateFormat.getDateInstance(DateFormat.SHORT).format(date.getTime());

        date.setTimeInMillis(fineObiettivoMs);
        String fineObiettivoString = DateFormat.getDateInstance(DateFormat.SHORT).format(date.getTime());

        String dateRangeFormat = getResources().getString(R.string.dateRangeFormat);
        String dateRangeString = String.format(dateRangeFormat, inizioObiettivoString, fineObiettivoString);

        final TextView dateRangeField = (TextView) findViewById(R.id.dateRange);
        dateRangeField.setText(dateRangeString);

        final List<MyBudgets> myBudgets = _db.ottieniTuttiObiettivi(inizioObiettivoMs, fineObiettivoMs);
        final MyBudgetsAdapter adattatoreListaObiettivi = new MyBudgetsAdapter(this, myBudgets);
        listaObiettivi.setAdapter(adattatoreListaObiettivi);

        registerForContextMenu(listaObiettivi);

        listaObiettivi.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                MyBudgets myBudgets = (MyBudgets)parent.getItemAtPosition(position);
                if(myBudgets == null)
                {
                    Log.w(TAG, "Puntatore " + position + "null");
                    return;
                }

                Intent i = new Intent(getApplicationContext(), TransazioneActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("budget", myBudgets.nome);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        MyBudgets blankMyBudgets = _db.ottieniObiettivoVuoto(inizioObiettivoMs, fineObiettivoMs);

        setupTotalEntry(myBudgets, blankMyBudgets);
    }

    private void setupTotalEntry(final List<MyBudgets> myBudgets, final MyBudgets blankMyBudgets)
    {
        final TextView nomeObiettivo = (TextView)findViewById(R.id.budgetName);
        final TextView valoreObiettivo = (TextView)findViewById(R.id.budgetValue);
        final ProgressBar completamentoObiettivo = (ProgressBar)findViewById(R.id.budgetBar);

        nomeObiettivo.setText(R.string.totalBudgetTitle);

        int max = 0;
        int current = 0;

        for(MyBudgets myBudget : myBudgets)
        {
            max += myBudget.max;
            current += myBudget.elementoAttuale;
        }

        current += blankMyBudgets.elementoAttuale;

        completamentoObiettivo.setMax(max);
        completamentoObiettivo.setProgress(current);

        String fraction = String.format(getResources().getString(R.string.fraction), current, max);
        valoreObiettivo.setText(fraction);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.list)
        {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.view_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ListView listView = (ListView) findViewById(R.id.list);

        if(info != null)
        {
            MyBudgets myBudgets = (MyBudgets) listView.getItemAtPosition(info.position);

            if (myBudgets != null && item.getItemId() == R.id.action_edit)
            {
                Intent i = new Intent(getApplicationContext(), MyBudgetsViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", myBudgets.nome);
                bundle.putBoolean("view", true);
                i.putExtras(bundle);
                startActivity(i);

                return true;
            }
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        getMenuInflater().inflate(R.menu.budget_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_add)
        {
            Intent i = new Intent(getApplicationContext(), MyBudgetsViewActivity.class);
            startActivity(i);
            return true;
        }

        if(id == R.id.action_calendar)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.budgetDateRangeHelp);

            final View view = getLayoutInflater().inflate(R.layout.budget_date_picker_layout, null, false);

            builder.setView(view);
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.cancel();
                }
            });
            builder.setPositiveButton(R.string.set, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    DatePicker startDatePicker = (DatePicker) view.findViewById(R.id.startDate);
                    DatePicker endDatePicker = (DatePicker) view.findViewById(R.id.endDate);

                    long startOfBudgetMs = GestoreCalendario.getStartOfDayMs(startDatePicker.getYear(),
                            startDatePicker.getMonth(), startDatePicker.getDayOfMonth());
                    long endOfBudgetMs = GestoreCalendario.getEndOfDayMs(endDatePicker.getYear(),
                            endDatePicker.getMonth(), endDatePicker.getDayOfMonth());

                    if (startOfBudgetMs > endOfBudgetMs)
                    {
                        Toast.makeText(MyBudgetsActivity.this, R.string.startDateAfterEndDate, Toast.LENGTH_LONG).show();
                        return;
                    }

                    Intent intent = new Intent(MyBudgetsActivity.this, MyBudgetsActivity.class);
                    intent.setFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);

                    Bundle bundle = new Bundle();
                    bundle.putLong("inizioObiettivo", startOfBudgetMs);
                    bundle.putLong("fineObiettivo", endOfBudgetMs);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    MyBudgetsActivity.this.finish();
                }
            });

            builder.show();
            return true;
        }

        if(id == android.R.id.home)
        {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy()
    {
        _db.close();
        super.onDestroy();
    }
}