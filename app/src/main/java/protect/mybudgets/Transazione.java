package protect.mybudgets;

import android.database.Cursor;

public class Transazione
{
    public final int id;
    public final int tipo;
    public final String descrizione;
    public final String account;
    public final String obiettivo;
    public final double valore;
    public final String note;
    public final long dateMs;
    public final String addon;

    private Transazione(final int id, final int tipo, final String descrizione, final String account,
                        final String obiettivo, final double valore, final String note, final long dateMs,
                        final String addon)
    {
        this.id = id;
        this.tipo = tipo;
        this.descrizione = descrizione;
        this.account = account;
        this.obiettivo = obiettivo;
        this.valore = valore;
        this.note = note;
        this.dateMs = dateMs;
        this.addon = addon;
    }

    private static String creaTransazioneBlank(final String string)
    {
        if(string != null)
        {
            return string;
        }
        else
        {
            return "";
        }
    }

    public static Transazione creaTransazione(Cursor cursor)
    {
        int id = cursor.getInt(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.NOME));
        int tipo = cursor.getInt(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.TIPO));
        String descrizione = cursor.getString(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.DESCRIZIONE));
        String account = cursor.getString(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.ACCOUNT));
        String obiettivo = cursor.getString(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.OBIETTIVO));
        double valore = cursor.getDouble(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.VALORE));
        String note = cursor.getString(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.NOTE));
        long dateMs = cursor.getLong(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.DATE));
        String addon = cursor.getString(cursor.getColumnIndexOrThrow(GestoreDatabase.TransactionDbIds.RICETTA));

        return new Transazione(id, tipo, creaTransazioneBlank(descrizione), creaTransazioneBlank(account),
                creaTransazioneBlank(obiettivo), valore, creaTransazioneBlank(note), dateMs,
                creaTransazioneBlank(addon));
    }
}