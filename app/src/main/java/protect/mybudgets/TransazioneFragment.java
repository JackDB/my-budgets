package protect.mybudgets;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class TransazioneFragment extends Fragment
{
    private final static String TAG = "BudgetWatch";

    private int _transactionType;
    private GestoreDatabase _db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Bundle arguments = getArguments();
        if(arguments == null || arguments.getInt("type", -1) == -1)
        {
            throw new IllegalStateException("Argomento mancante.");
        }

        _transactionType = arguments.getInt("type");
        _db = new GestoreDatabase(getContext());


        final Bundle b = getActivity().getIntent().getExtras();
        final String budgetToDisplay = b != null ? b.getString("budget", null) : null;

        final String searchToUse = arguments.getString("search", null);

        View layout = inflater.inflate(R.layout.list_layout, container, false);
        ListView listView = (ListView) layout.findViewById(R.id.list);
        final TextView helpText = (TextView) layout.findViewById(R.id.helpText);

        Cursor cursor = _db.ottieniTutteTransazioni(_transactionType, budgetToDisplay, searchToUse, null, null);

        if(cursor.getCount() > 0)
        {
            listView.setVisibility(View.VISIBLE);
            helpText.setVisibility(View.GONE);
        }
        else
        {
            listView.setVisibility(View.GONE);
            helpText.setVisibility(View.VISIBLE);

            String message;

            if(searchToUse == null)
            {
                if(budgetToDisplay == null)
                {
                    int stringId = (_transactionType == GestoreDatabase.TransactionDbIds.SPESA) ?
                            R.string.noExpenses : R.string.noRevenues;
                    message = getResources().getString(stringId);
                }
                else
                {
                    int stringId = (_transactionType == GestoreDatabase.TransactionDbIds.SPESA) ?
                            R.string.noExpensesForBudget : R.string.noRevenuesForBudget;
                    String base = getResources().getString(stringId);
                    message = String.format(base, budgetToDisplay);
                }
            }
            


        }

        final AdapterNuovaTransazione adapter = new AdapterNuovaTransazione(getContext(), cursor);
        listView.setAdapter(adapter);

        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Cursor selected = (Cursor)parent.getItemAtPosition(position);
                if(selected == null)
                {
                    
                    return;
                }

                Transazione transazione = Transazione.creaTransazione(selected);

                Intent i = new Intent(view.getContext(), TransazioneViewActivity.class);
                final Bundle b = new Bundle();
                b.putInt("id", transazione.id);
                b.putInt("type", _transactionType);
                b.putBoolean("view", true);
                i.putExtras(b);
                startActivity(i);
            }
        });

        return layout;
    }

    @Override
    public void onDestroyView()
    {
        _db.close();
        super.onDestroyView();
    }
}
