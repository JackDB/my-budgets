package protect.mybudgets;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class CheckerModificheDB extends BroadcastReceiver
{
    public static final String DATABASE_CAMBIATO = "protect.mybudgets.DATABASE_CAMBIATO";

    private boolean _databaseCambiato = false;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        _databaseCambiato = true;
    }

    public boolean databaseCambiato()
    {
        return _databaseCambiato;
    }

    public void reset()
    {
        _databaseCambiato = false;
    }
}