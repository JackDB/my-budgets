package protect.mybudgets;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class TransazioneViewActivity extends AppCompatActivity
{
    private static final String TAG = "MyBudgets";
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int PERMISSIONS_REQUEST_CAMERA = 2;

    static final String AGGIUNGI_NUOVA_SPESA = "AggiungiNuovaSpesa";
    static final String AGGIUNGI_NUOVO_GUADAGNO = "AggiungiNuovoGuadagno";

    private String transazioneNonValida = null;
    private GestoreDatabase _db;

    private EditText _editorNome;
    private TextView _visualizzaNome;
    private EditText _modificaAccount;
    private TextView _visualizzaAccount;
    private EditText _modificaNome;
    private TextView _visualizzaValore;
    private EditText _modificaNote;
    private TextView _visualizzaNote;
    private TextView _visualizzaObiettivi;
    private TextView _visualizzaData;
    private Button _bottoneVista;
    private View _layoutObiettivi;
    private View _divisoreFinale;
    private TextView _fieldTransazioni;
    private View _layoutNessunObiettivo;
    private View _layoutObiettiviPresenti;
    private EditText _modificaData;
    private Spinner _spinnerObiettivo;

    private int _IDTransazione;
    private int _tipo;
    private boolean _aggiornaTransazione;
    private boolean _visualizzaTransazione;

    private void extractIntentFields(Intent intent)
    {
        final Bundle b = intent.getExtras();
        String action = intent.getAction();
        if(b != null)
        {
            _IDTransazione = b.getInt("id");
            _tipo = b.getInt("type");
            _aggiornaTransazione = b.getBoolean("update", false);
            _visualizzaTransazione = b.getBoolean("view", false);
        }
        else if(action != null)
        {
            _aggiornaTransazione = false;
            _visualizzaTransazione = false;

            if(action.equals(AGGIUNGI_NUOVA_SPESA))
            {
                _tipo = GestoreDatabase.TransactionDbIds.SPESA;
            }
            else if(action.equals(AGGIUNGI_NUOVO_GUADAGNO))
            {
                _tipo = GestoreDatabase.TransactionDbIds.GUADAGNO;
            }
            else
            {

                finish();
            }
        }
        else
        {

            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.transaction_view_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        _db = new GestoreDatabase(this);

        _editorNome = (EditText) findViewById(R.id.nameEdit);
        _visualizzaNome = (TextView) findViewById(R.id.nameView);
        _modificaAccount = (EditText) findViewById(R.id.accountEdit);
        _visualizzaAccount = (TextView) findViewById(R.id.accountView);
        _modificaNome = (EditText) findViewById(R.id.valueEdit);
        _visualizzaValore = (TextView) findViewById(R.id.valueView);
        _modificaNote = (EditText) findViewById(R.id.noteEdit);
        _visualizzaNote = (TextView) findViewById(R.id.noteView);
        _visualizzaObiettivi = (TextView) findViewById(R.id.budgetView);
        _visualizzaData = (TextView) findViewById(R.id.dateView);
        _bottoneVista = (Button)findViewById(R.id.viewButton);
        _layoutObiettivi = findViewById(R.id.receiptLayout);
        _divisoreFinale = findViewById(R.id.endingDivider);
        _fieldTransazioni = (TextView) findViewById(R.id.receiptLocation);
        _layoutNessunObiettivo = findViewById(R.id.noReceiptButtonLayout);
        _layoutObiettiviPresenti = findViewById(R.id.hasReceiptButtonLayout);
        _modificaData = (EditText) findViewById(R.id.dateEdit);
        _spinnerObiettivo = (Spinner) findViewById(R.id.budgetSpinner);

        extractIntentFields(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent)
    {

        extractIntentFields(intent);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onResume()
    {
        super.onResume();

        if(_tipo == GestoreDatabase.TransactionDbIds.SPESA)
        {
            if (_aggiornaTransazione)
            {
                setTitle(R.string.editExpenseTransactionTitle);
            }
            else if (_visualizzaTransazione)
            {
                setTitle(R.string.viewExpenseTransactionTitle);
            }
            else
            {
                setTitle(R.string.addExpenseTransactionTitle);
            }
        }
        else if(_tipo == GestoreDatabase.TransactionDbIds.GUADAGNO)
        {
            if(_aggiornaTransazione)
            {
                setTitle(R.string.editRevenueTransactionTitle);
            }
            else if(_visualizzaTransazione)
            {
                setTitle(R.string.viewRevenueTransactionTitle);
            }
            else
            {
                setTitle(R.string.addRevenueTransactionTitle);
            }
        }

        final Calendar date = new GregorianCalendar();
        final DateFormat dateFormatter = SimpleDateFormat.getDateInstance();

        _modificaData.setText(dateFormatter.format(date.getTime()));

        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day)
            {
                date.set(year, month, day);
                _modificaData.setText(dateFormatter.format(date.getTime()));
            }
        };

        _modificaData.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus)
                {
                    int year = date.get(Calendar.YEAR);
                    int month = date.get(Calendar.MONTH);
                    int day = date.get(Calendar.DATE);
                    DatePickerDialog datePicker = new DatePickerDialog(TransazioneViewActivity.this,
                            dateSetListener, year, month, day);
                    datePicker.show();
                }
            }
        });

        List<String> nomiObiettiviAttuali = _db.ottieniNomiObiettivi();
        LinkedList<String> nomiObiettivi = new LinkedList<>(nomiObiettiviAttuali);


        nomiObiettivi.addFirst("");


        if(_spinnerObiettivo.getCount() == 0)
        {
            ArrayAdapter<String> obiettivi = new ArrayAdapter<>(this, R.layout.spinner_textview, nomiObiettivi);
            _spinnerObiettivo.setAdapter(obiettivi);
        }

        if(_aggiornaTransazione || _visualizzaTransazione)
        {
            Transazione transazione = _db.ottieniTransazione(_IDTransazione);
            (_aggiornaTransazione ? _editorNome : _visualizzaNome).setText(transazione.descrizione);
            (_aggiornaTransazione ? _modificaAccount : _visualizzaAccount).setText(transazione.account);

            int budgetIndex = nomiObiettivi.indexOf(transazione.obiettivo);
            if(budgetIndex >= 0)
            {
                _spinnerObiettivo.setSelection(budgetIndex);
            }
            _visualizzaObiettivi.setText(_visualizzaTransazione ? transazione.obiettivo : "");

            (_aggiornaTransazione ? _modificaNome : _visualizzaValore).setText(String.format(Locale.US, "%.2f", transazione.valore));
            (_aggiornaTransazione ? _modificaNote : _visualizzaNote).setText(transazione.note);
            (_aggiornaTransazione ? _modificaData : _visualizzaData).setText(dateFormatter.format(new Date(transazione.dateMs)));
            _fieldTransazioni.setText(transazione.addon);

            if(_visualizzaTransazione)
            {
                _spinnerObiettivo.setVisibility(View.GONE);
                _editorNome.setVisibility(View.GONE);
                _modificaAccount.setVisibility(View.GONE);
                _modificaNome.setVisibility(View.GONE);
                _modificaNote.setVisibility(View.GONE);
                _modificaData.setVisibility(View.GONE);


                _layoutNessunObiettivo.setVisibility(View.GONE);


                if(transazione.addon.isEmpty() == false)
                {
                    _layoutObiettivi.setVisibility(View.VISIBLE);
                    _divisoreFinale.setVisibility(View.VISIBLE);
                    _layoutObiettiviPresenti.setVisibility(View.VISIBLE);
                }
                else
                {
                    _layoutObiettivi.setVisibility(View.GONE);
                    _divisoreFinale.setVisibility(View.GONE);
                }
            }
            else
            {
                _visualizzaObiettivi.setVisibility(View.GONE);
                _visualizzaNome.setVisibility(View.GONE);
                _visualizzaAccount.setVisibility(View.GONE);
                _visualizzaValore.setVisibility(View.GONE);
                _visualizzaNote.setVisibility(View.GONE);
                _visualizzaData.setVisibility(View.GONE);

                // If editing a transaction, always list the receipt field
                _layoutObiettivi.setVisibility(View.VISIBLE);
                _divisoreFinale.setVisibility(View.VISIBLE);
                if(transazione.addon.isEmpty() && transazioneNonValida == null)
                {
                    _layoutNessunObiettivo.setVisibility(View.VISIBLE);
                    _layoutObiettiviPresenti.setVisibility(View.GONE);
                }
                else
                {
                    _layoutNessunObiettivo.setVisibility(View.GONE);
                    _layoutObiettiviPresenti.setVisibility(View.VISIBLE);

                }
            }
        }
        else
        {
            _visualizzaObiettivi.setVisibility(View.GONE);
            _visualizzaNome.setVisibility(View.GONE);
            _visualizzaAccount.setVisibility(View.GONE);
            _visualizzaValore.setVisibility(View.GONE);
            _visualizzaNote.setVisibility(View.GONE);
            _visualizzaData.setVisibility(View.GONE);

            
            _layoutObiettivi.setVisibility(View.VISIBLE);
            _divisoreFinale.setVisibility(View.VISIBLE);
            if(transazioneNonValida == null)
            {
                _layoutNessunObiettivo.setVisibility(View.VISIBLE);
                _layoutObiettiviPresenti.setVisibility(View.GONE);
            }
            else
            {
                _layoutNessunObiettivo.setVisibility(View.GONE);
                _layoutObiettiviPresenti.setVisibility(View.VISIBLE);

            }
        }






    }

    private void salvataggioDati()
    {
        final String name = _editorNome.getText().toString();


        final String budget = (String) _spinnerObiettivo.getSelectedItem();
        if (budget == null)
        {
            Snackbar.make(_spinnerObiettivo, R.string.budgetMissing, Snackbar.LENGTH_LONG).show();
            return;
        }

        final String account = _modificaAccount.getText().toString();


        final String valueStr = _modificaNome.getText().toString();
        if (valueStr.isEmpty())
        {
            Snackbar.make(_modificaNome, R.string.valueMissing, Snackbar.LENGTH_LONG).show();
            return;
        }

        double value;
        try
        {
            value = Double.parseDouble(valueStr);
        }
        catch (NumberFormatException e)
        {
            Snackbar.make(_modificaNome, R.string.valueInvalid, Snackbar.LENGTH_LONG).show();
            return;
        }

        final String note = _modificaNote.getText().toString();


        final String dateStr = _modificaData.getText().toString();
        final DateFormat dateFormatter = SimpleDateFormat.getDateInstance();
        long dateMs;
        try
        {
            dateMs = dateFormatter.parse(dateStr).getTime();
        }
        catch (ParseException e)
        {
            Snackbar.make(_modificaData, R.string.dateInvalid, Snackbar.LENGTH_LONG).show();
            return;
        }

        String elemento = _fieldTransazioni.getText().toString();
        if(transazioneNonValida != null)
        {

            File oldReceipt = new File(elemento);
            if(oldReceipt.delete() == false)
            {
                Log.e(TAG, "Eliminazione non completata: " + transazioneNonValida);
            }


            elemento = transazioneNonValida;
            transazioneNonValida = null;
        }

        if(_aggiornaTransazione)
        {
            _db.aggiornaTransazione(_IDTransazione, _tipo, name, account,
                    budget, value, note, dateMs, elemento);

        }
        else
        {
            _db.inserisciTransazione(_tipo, name, account, budget,
                    value, note, dateMs, elemento);
        }

        finish();
    }



    @Override
    protected void onDestroy()
    {
        if(transazioneNonValida != null)
        {
            //Nulla da eseguire.

        }

        _db.close();

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        if(_visualizzaTransazione)
        {
            getMenuInflater().inflate(R.menu.view_menu, menu);
        }
        else if(_aggiornaTransazione)
        {
            getMenuInflater().inflate(R.menu.edit_menu, menu);
        }
        else
        {
            getMenuInflater().inflate(R.menu.add_menu, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if(id == R.id.action_save)
        {
            salvataggioDati();
            return true;
        }

        if(id == android.R.id.home)
        {
            finish();
            return true;
        }

        if(id == R.id.action_edit)
        {
            finish();

            Intent i = new Intent(getApplicationContext(), TransazioneViewActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("id", _IDTransazione);
            bundle.putInt("type", _tipo);
            bundle.putBoolean("update", true);
            i.putExtras(bundle);
            startActivity(i);
            return true;
        }

        if(id == R.id.action_delete)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.deleteTransactionTitle);
            builder.setMessage(R.string.deleteTransactionConfirmation);
            builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    Log.e(TAG, "Deleting transaction: " + _IDTransazione);

                    _db.eliminaTransazione(_IDTransazione);
                    finish();

                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }








}
