package protect.mybudgets;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

class AdapterNuovaTransazione extends CursorAdapter
{
    public AdapterNuovaTransazione(Context context, Cursor cursor)
    {
        super(context, cursor, 0);
    }

    private final DateFormat DATE_FORMATTER = SimpleDateFormat.getDateInstance();

    static class ViewHolder
    {
        TextView fieldNome;
        TextView fieldValore;
        TextView fieldData;
        TextView fieldObiettivo;
        ImageView iconaRicetta;
        TextView note;
        View noteLayout;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.transaction_layout, parent, false);

        ViewHolder holder = new ViewHolder();
        holder.fieldNome = (TextView) view.findViewById(R.id.name);
        holder.fieldValore = (TextView) view.findViewById(R.id.value);
        holder.fieldData = (TextView) view.findViewById(R.id.date);
        holder.fieldObiettivo = (TextView) view.findViewById(R.id.myBudgets);
        holder.iconaRicetta = (ImageView) view.findViewById(R.id.receiptIcon);
        holder.note = (TextView) view.findViewById(R.id.note);
        holder.noteLayout = view.findViewById(R.id.noteLayout);
        view.setTag(holder);

        return view;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        ViewHolder holder = (ViewHolder)view.getTag();


        Transazione transazione = Transazione.creaTransazione(cursor);


        holder.fieldNome.setText(transazione.descrizione);
        holder.fieldValore.setText(String.format(Locale.US, "%.2f", transazione.valore));
        holder.fieldObiettivo.setText(transazione.obiettivo);

        holder.fieldData.setText(DATE_FORMATTER.format(transazione.dateMs));

        if(transazione.addon.isEmpty())
        {
            holder.iconaRicetta.setVisibility(View.GONE);
        }
        else
        {
            holder.iconaRicetta.setVisibility(View.VISIBLE);
        }

        if(transazione.note.isEmpty())
        {
            holder.noteLayout.setVisibility(View.GONE);
            holder.note.setText("");
        }
        else
        {
            holder.noteLayout.setVisibility(View.VISIBLE);
            holder.note.setText(transazione.note);
        }
    }
}
