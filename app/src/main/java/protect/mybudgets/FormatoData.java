package protect.mybudgets;

public enum FormatoData
{
    CSV("text/csv"),
    JSON("application/json"),
    ZIP("application/zip"),
    ;

    private final String mimetype;

    FormatoData(String mimetype)
    {

        this.mimetype = mimetype;
    }


    public String extension()
    {

        return this.name().toLowerCase();
    }


    public String mimetype()
    {
        return mimetype;
    }
}
