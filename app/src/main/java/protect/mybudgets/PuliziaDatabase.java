package protect.mybudgets;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;

class PuliziaDatabase extends AsyncTask<Void, Void, Void>
{
    private static final String TAG = "MyBudgets";

    private final Activity activity;
    private final Long CutOffObiettivi;

    private ProgressDialog progress;

    public PuliziaDatabase(Activity activity)
    {
        super();
        this.activity = activity;
        CutOffObiettivi = null;
    }

    public PuliziaDatabase(Activity activity, long CutOffObiettivi)
    {
        super();
        this.activity = activity;
        this.CutOffObiettivi = CutOffObiettivi;
    }

    protected void onPreExecute()
    {
        progress = new ProgressDialog(activity);
        progress.setTitle(R.string.cleaning);

        progress.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                PuliziaDatabase.this.cancel(true);
            }
        });

        progress.show();
    }

    private void rimuoviTransazioni(GestoreDatabase db)
    {
        Cursor iteratore = db.ottieniTransazioniParticolari(CutOffObiettivi);

        while(iteratore.moveToNext())
        {
            Transazione transazione = Transazione.creaTransazione(iteratore);
            File elemento = new File(transazione.addon);
            boolean result = elemento.delete();
            if(result == false)
            {
                Log.i(TAG, "Eliminazione fallita: " + transazione.id);
            }

            db.aggiornaTransazione(transazione.id, transazione.tipo, transazione.descrizione,
                    transazione.account, transazione.obiettivo, transazione.valore, transazione.note,
                    transazione.dateMs, "");
        }
        iteratore.close();
    }

    private void correggiTransazione(GestoreDatabase db)
    {
        Cursor iteratore = db.ottieniTransazioniParticolari(null);

        while(iteratore.moveToNext())
        {
            Transazione transazione = Transazione.creaTransazione(iteratore);
            if(transazione.addon.isEmpty() == false)
            {
                File elemento = new File(transazione.addon);
                if(elemento.isFile() == false)
                {

                    db.aggiornaTransazione(transazione.id, transazione.tipo, transazione.descrizione,
                            transazione.account, transazione.obiettivo, transazione.valore, transazione.note,
                            transazione.dateMs, /* no receipt */ "");

                }
            }
        }
        iteratore.close();
    }

    private void eliminaTransazioniInutili(GestoreDatabase db)
    {
        File imageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if(imageDir == null || imageDir.exists() == false)
        {
            // Nulla da eliminare
            return;
        }

        Cursor iteratore = db.ottieniTransazioniParticolari(null);

        File [] files = imageDir.listFiles();
        if(files == null)
        {

            files = new File[0];
        }

        for(File elemento : files)
        {

            boolean trovato = false;
            iteratore.moveToPosition(-1);
            while(iteratore.moveToNext())
            {
                Transazione transazione = Transazione.creaTransazione(iteratore);
                File transactionReceipt = new File(transazione.addon);
                if(transactionReceipt.equals(elemento))
                {

                    trovato = true;
                    break;
                }
            }

            if(trovato == false)
            {

                boolean risultatoOperazione = elemento.delete();
                if(risultatoOperazione == false)
                {
                    Log.w(TAG, "Eliminazione fallita: " + elemento.getAbsolutePath());
                }
            }
        }

        iteratore.close();
    }

    protected Void doInBackground(Void... nothing)
    {
        GestoreDatabase db = new GestoreDatabase(activity);

        if(CutOffObiettivi != null)
        {
            rimuoviTransazioni(db);
        }

        correggiTransazione(db);
        eliminaTransazioniInutili(db);

        db.close();

        return null;
    }

    protected void onPostExecute(Void result)
    {
        progress.dismiss();
        Log.i(TAG, "Pulizia Completata.");
    }

    protected void onCancelled()
    {
        progress.dismiss();
        Log.i(TAG, "Pulizia Annullata.");
    }
}
