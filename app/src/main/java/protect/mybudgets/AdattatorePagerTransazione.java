package protect.mybudgets;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

class AdattatorePagerTransazione extends FragmentStatePagerAdapter
{
    private final int numTabs;
    private final String search;

    public AdattatorePagerTransazione(FragmentManager fm, String search, int numTabs)
    {
        super(fm);
        this.search = search;
        this.numTabs = numTabs;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment = new TransazioneFragment();
        Bundle arguments = new Bundle();
        int transactionType = (position == 0) ?
                GestoreDatabase.TransactionDbIds.SPESA : GestoreDatabase.TransactionDbIds.GUADAGNO;
        arguments.putInt("type", transactionType);
        if(search != null)
        {
            arguments.putString("search", search);
        }

        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public int getCount()
    {
        return numTabs;
    }
}
