package protect.mybudgets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

class MyBudgetsAdapter extends ArrayAdapter<MyBudgets>
{
    private final String FRACTION_FORMAT;

    public MyBudgetsAdapter(Context context, List<MyBudgets> items)
    {
        super(context, 0, items);

        FRACTION_FORMAT = context.getResources().getString(R.string.fraction);
    }

    static class ViewHolder
    {
        TextView nomeObiettivo;
        ProgressBar progressoObiettivo;
        TextView valoreObiettivo;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        MyBudgets item = getItem(position);

        ViewHolder holder;



        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.budget_layout,
                    parent, false);

            holder = new ViewHolder();
            holder.nomeObiettivo = (TextView) convertView.findViewById(R.id.budgetName);
            holder.progressoObiettivo = (ProgressBar) convertView.findViewById(R.id.budgetBar);
            holder.valoreObiettivo = (TextView) convertView.findViewById(R.id.budgetValue);
            convertView.setTag(holder);

            
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.nomeObiettivo.setText(item.nome);

        holder.progressoObiettivo.setMax(item.max);
        holder.progressoObiettivo.setProgress(item.elementoAttuale);

        String fraction = String.format(FRACTION_FORMAT, item.elementoAttuale, item.max);

        holder.valoreObiettivo.setText(fraction);

        return convertView;
    }
}
